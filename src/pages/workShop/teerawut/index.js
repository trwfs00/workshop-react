import { Container, Typography } from "@mui/material";
import { Layout } from "src/layouts/dashboard/layout";
import Introduction from "src/components/freshy/introduction";
import { Inter } from 'next/font/google'
import Navigation from "src/components/freshy/navigation";


const inter = Inter({ subsets: ['latin'] })

const teerawutProfile = {
    nickName: "Freshy",
    fullName: "Teerawut Sungkagaro",
    avatar: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/avatar-freshy.png?alt=media&token=55cc979e-a0db-4017-816f-a4ae58010725',
    biography: `Bachelor of Information Technology, College of Computing - Khon Kaen University, 
    Thailand. Interesting to know more about programming and explore experiences.`,
    contacts: {
        github: "https://github.com/trwfs00",
        linkedin: "https://www.linkedin.com/in/trwfs00/",
        dribbble: "https://dribbble.com/trwfs0"
    }
}

const teerawut = () => {
    return (
        <>
            <main className={`flex min-h-screen flex-col items-center justify-between p-12 md:p-24 ${inter.className}`}>
                <Container maxWidth="xl">
                    <Introduction
                        profile={teerawutProfile}
                    />
                    <Navigation />
                </Container>
            </main>
        </>
    )
}
teerawut.getLayout = (teerawut) => <Layout>{teerawut}</Layout>;
export default teerawut;